const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth')
const gameCtrl = require('../controllers/gameController');

router.get('/', gameCtrl.getAllGames);
router.get('/computerPlay', gameCtrl.computerPlay);
router.post('/whoWon', gameCtrl.whoIsTheWinner);

module.exports = router;