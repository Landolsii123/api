const Game = require('../models/game');

const ROCK = 'ROCK';
const PAPER = 'PAPER';
const SCISSORS = 'SCISSORS';
const computer = { name: 'computer', isComputer: true };
const human = { name: 'human' };
const MOVES = [ROCK, PAPER, SCISSORS];

let computerPlayPromise = new Promise((myResolve, myReject) => {
    let computerMove = MOVES[Math.floor(Math.random() * 3)];
    
    // The producing code (this may take some time)
  
    if (computerMove === ROCK || computerMove === PAPER || computerMove === SCISSORS) {
      myResolve(computerMove);
    } else {
      myReject("Wrong move");
    }
});

const winnerPromise = (humanMove, computerMove) => {
    return new Promise((resolve, reject)=>{
        
        if (humanMove === computerMove) {
            resolve(false);
        } else {
            if (humanMove === SCISSORS) {
              if (computerMove === ROCK) {
                resolve(computer) ;
              } else {
                resolve(human) ;
              }
            }
            if (humanMove === PAPER) {
              if (computerMove === ROCK) {
                resolve(human) ;
              } else {
                resolve(computer) ;
              }
            }
            if (humanMove === ROCK) {
              if (computerMove === SCISSORS) {
                resolve(human) ;
              } else {
                resolve(computer) ;
              }
            }
        }
        reject(Error("It broke"));
        
    });
}

exports.getAllGames = (req, res, next) => {
    Game.find().then(
        (games) => {
            res.status(200).json(games);
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};

exports.computerPlay = (req, res, next) => {
    computerPlayPromise.then(computerMove=>{
        console.log(computerMove);
        res.status(200).json(computerMove)
    })
};

exports.whoIsTheWinner = (req, res, next) => {
    let humanMove = req.body.humanMove;
    let computerMove = req.body.computerMove;
    winnerPromise(humanMove,computerMove).then(winner=>res.status(200).json(winner))
};



