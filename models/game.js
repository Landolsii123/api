const mongoose = require('mongoose');

const gameSchema = mongoose.Schema({
    computerMove: { type: String, required: true },
    humanMove: { type: String, required: true },
    winner: { type: String, required: true },
});

module.exports = mongoose.model('Game', gameSchema);